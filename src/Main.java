//Pewna firma planuje wprowadzić nowy produkt na rynek. Chciałaby, żeby reklama tego produktu
//        jak najlepiej była dopasowana do odbiorców. Dlatego też zebrali imiona i nazwiska
//        osób, które kupowały ich produkty. Postanowili w reklamie umieścić najpopularniejsze
//        imiona i nazwiska. Otrzymaliśmy zadanie przygotowania programu, który obliczy
//        ile razy dane imię i nazwisko pojawia się w liście. Przy kopiowaniu wkradły się błędy.
//        Część liter jest z dużej litery. Oczywiście nasz program powinien poradzić sobie z tym.
//        Wyświetlamy dane w formacie (imię i nazwisko z dużej litery - reszta małymi):
//        Imię, ilość wystąpień
//        Imię, ilość wystąpień
//        Imię, ilość wystąpień
//
//        Nazwisko, ilość wystąpień
//        Nazwisko, ilość wystąpień

import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<String> listOfNames = new ArrayList();
        List<String> listOfSurnames = new ArrayList();

        try (FileReader readerF = new FileReader("ListOfNames.txt");
             BufferedReader reader = new BufferedReader(readerF)) {
            String linia = null;
            while (true) {
                linia = reader.readLine();
                if (linia == null) break;
                linia = linia.toLowerCase();
                String[] words = linia.split(" ");
                listOfNames.add(firstUpLetter(words[1]));
                listOfSurnames.add(firstUpLetter(words[0]));
//                System.out.println("Linia: " + linia);
            }
        } catch (FileNotFoundException fnfe) {
            System.out.println("Nie ma takiego pliku.");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map<String, Integer> mapaImion = new HashMap<>();
        Map<String, Integer> mapaNazwisk = new HashMap<>();

        countWystapienia(listOfNames, mapaImion);
        countWystapienia(listOfSurnames, mapaNazwisk);

        for (Map.Entry<String, Integer> x : mapaImion.entrySet()) {
            System.out.println("imie: " + x.getKey() + " wystapien " + x.getValue());
        }
        for (Map.Entry<String, Integer> x : mapaNazwisk.entrySet()) {
            System.out.println("nazwisko: " + x.getKey() + " wystapien " + x.getValue());
        }
    }

    private static void countWystapienia(List<String> listOfSurnames, Map<String, Integer> mapaNazwisk) {
        for (String x : listOfSurnames) {
            if (mapaNazwisk.containsKey(x)) {
                mapaNazwisk.put(x, mapaNazwisk.get(x) + 1);
            } else mapaNazwisk.put(x, 1);
        }
    }

    public static String firstUpLetter(String x) {
        char y = (char) ((int) x.charAt(0) -32);
        return y + x.substring(1);
    }

}


